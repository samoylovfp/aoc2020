use std::fs::read_to_string;

fn main() {
    let input = read_to_string("input.txt").unwrap();
    println!("First part is {}", part_one(&input));
    // println!("Second part is {}", part_two(&input));
}

fn part_one(input: &str) -> u64 {
    0
}

fn part_two(input: &str) -> u64 {
    0
}


#[test]
fn test_part_one() {
    assert_eq!(part_one(r#"

    "#), 1)
}
