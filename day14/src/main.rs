#![feature(str_split_once)]

use std::{collections::HashMap, fs::read_to_string};
use itertools::Itertools;

fn main() {
    let input = read_to_string("input.txt").unwrap();
    println!("Part one is {}", part_one(&input));
    println!("Part two is {}", part_two(&input));
}


fn part_one(input: &str) -> u64 {
    let mut mem: HashMap<usize, u64>= HashMap::new();
    let mut mul = u64::MAX;
    let mut add = 0u64;

    for line in input.trim().lines() {
        let (instr, value) = line.trim().split_once(" = ").unwrap();
        if instr == "mask" {
            mul = u64::from_str_radix(&value.replace("X", "1"), 2).unwrap();
            add = u64::from_str_radix(&value.replace("X", "0"), 2).unwrap();
        } else {
            let addr = instr[4..instr.len() - 1].parse().unwrap();
            let masked_value = u64::from_str_radix(value, 10).unwrap() & mul | add;
            // println!("{}: {} & {:b} | {:b} = {:b}", addr, value, mul, add, masked_value);
            mem.insert(addr,  masked_value);
        }
    }

    mem.values().sum()
}

fn parse_bin(v: &str) -> u64 {
    u64::from_str_radix(v, 2).unwrap()
}


fn part_two(input: &str) -> u64 {
    let mut mem: HashMap<usize, u64>= HashMap::new();
    let mut addr_static_mask = 0;
    let mut addr_dynamic_mask_cutoff = 0;
    let mut addr_dynamic_masks = Vec::new();
    for line in input.trim().lines() {
        let (instr, value) = line.trim().split_once(" = ").unwrap();
        if instr == "mask" {
            addr_static_mask = parse_bin(&value.replace("X", "0"));
            addr_dynamic_mask_cutoff = parse_bin(&value.replace("0", "1").replace("X", "0"));
            addr_dynamic_masks = value.chars().rev().enumerate().filter_map(|(i, c)|{
                match c {
                    'X' => Some(2u64.pow(i as u32)),
                    _ => None
                }
            }).collect();
        } else {
            let mut static_addr: u64 = instr[4..instr.len() - 1].parse().unwrap();
            static_addr = !(!static_addr & !addr_static_mask) & addr_dynamic_mask_cutoff;
            for n in 0..=addr_dynamic_masks.len() {
                for combination in addr_dynamic_masks.iter().combinations(n) {
                    let mut addr = static_addr;
                    for element in combination {
                        addr |= element;
                    }
                    // println!("{:b} gets {}", addr, value);
                    mem.insert(addr as usize, value.parse().unwrap());
                }
            }
        }
    }

    mem.values().sum()
}


#[test]
fn test_part_one() {
    assert_eq!(part_one(r#"
        mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
        mem[8] = 11
        mem[7] = 101
        mem[8] = 0
    "#), 165)
}


#[test]
fn test_part_two() {
    assert_eq!(part_two(r#"
        mask = 000000000000000000000000000000X1001X
        mem[42] = 100
        mask = 00000000000000000000000000000000X0XX
        mem[26] = 1
    "#), 208)
}
