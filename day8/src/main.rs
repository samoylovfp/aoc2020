#![feature(str_split_once)]

use std::fs::read_to_string;

#[derive(Debug, Eq, PartialEq)]
enum ComputationResult {
    Repeats(i32),
    Exits(i32)
}

use ComputationResult::*;

fn main() {
    let data = read_to_string("input.txt").unwrap();
    println!(
        "First part is {:?}",
        compute_till_repeats(&data)
    );
    println!(
        "Second part is {}",
        fix_and_compute(&data)
    )
}

fn compute_till_repeats(instructions: &str) -> i32 {
    let instructions: Vec<String> = instructions.trim().lines().map(|l|l.trim().to_owned()).collect();
    match execute(&instructions) {
        Exits(_) => panic!("Program didn't repeat"),
        Repeats(r) => r
    }
}

fn fix_and_compute(instructions: &str) -> i32 {
    let mut instructions: Vec<String> = instructions.trim().lines().map(|l|l.trim().to_owned()).collect();
    let mut mutating_instruction_pointer = 0;
    let mut result = execute(&instructions);
    while let Repeats(_) = result {
        if mutating_instruction_pointer > 0 {
            flip(&mut instructions[mutating_instruction_pointer - 1])
        }
        flip(&mut instructions[mutating_instruction_pointer]);
        mutating_instruction_pointer += 1;
        result = execute(&instructions);
    }
    match result {
        Repeats(_) => panic!("Didn't find the right instruction"),
        Exits(acc) => acc
    }
}

fn flip(instr_str: &mut String) {
    // let (instr, arg) = instr_str.split_once(" ").unwrap();
    match &instr_str[..3] {
        "jmp" => instr_str.replace_range(0..3, "nop"),
        "nop" => instr_str.replace_range(0..3, "jmp"),
        _ => {}
    }
}

fn execute(instructions: &[String]) -> ComputationResult {
    let mut visited = vec![false; instructions.len()];
    let mut accumulator = 0;
    let mut instruction_pointer = 0;
    while instruction_pointer < instructions.len() && !visited[instruction_pointer] {
        let (instr, argument) = instructions[instruction_pointer].split_once(" ").unwrap();
        visited[instruction_pointer] = true;
        match instr {
            "acc" => {
                accumulator += argument.parse::<i32>().unwrap();
                instruction_pointer += 1
            },
            "jmp" => {
                instruction_pointer = (instruction_pointer as i32 + argument.parse::<i32>().unwrap()) as usize;
            },
            _ => {
                instruction_pointer += 1
            }
        }
    }
    if instruction_pointer < instructions.len() {
        Repeats(accumulator)
    } else {
        Exits(accumulator)
    }
}

#[test]
fn test_part_one() {
    assert_eq!(
        compute_till_repeats(
            r#"nop +0
            acc +1
            jmp +4
            acc +3
            jmp -3
            acc -99
            acc +1
            jmp -4
            acc +6"#
        ),
        5
    );
}

#[test]
fn test_part_two() {
    assert_eq!(
        fix_and_compute(
            r#"nop +0
            acc +1
            jmp +4
            acc +3
            jmp -3
            acc -99
            acc +1
            jmp -4
            acc +6"#
        ),
        8
    );
}