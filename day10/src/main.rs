use std::{collections::HashSet, fs::read_to_string, process::exit};

fn main() {
    let input = read_to_string("input.txt").unwrap();
    println!("Part one is {}", joltage_diff_multiplied(&input));
    println!("Part two is {}", joltage_permutations(&input));
}

fn joltage_diff_multiplied(input: &str) -> u32 {
    let mut data: Vec<u32> = input
        .trim()
        .lines()
        .map(|l| l.trim().parse().unwrap())
        .collect();
    data.push(0);
    data.sort();
    data.push(data.last().unwrap() + 3);
    (data
        .windows(2)
        .map(|w| w[1] - w[0])
        .filter(|&c| c == 1)
        .count()
        * data
            .windows(2)
            .map(|w| w[1] - w[0])
            .filter(|&c| c == 3)
            .count()) as u32
}

fn joltage_permutations(input: &str) -> u64 {
    let mut data: Vec<u32> = input
        .trim()
        .lines()
        .map(|l| l.trim().parse().unwrap())
        .collect();
    data.push(0);
    data.sort();
    data.push(data.last().unwrap() + 3);
    let mut result = 1;
    let mut block = vec![];
    for e in data {
        if let Some(last_from_block) = block.last() {
            if e - last_from_block >= 3 {
                result *= block_permutations(&block);
                block.clear();
            } 
        }
        block.push(e)
    }
    if !block.is_empty() {
        result *= block_permutations(&block);
    }
    result
}

fn block_permutations(block: &[u32]) -> u64 {

    match block.len() {
        5 => 7,
        4 => 4,
        3 => 2,
        2 => 1,
        1 => 1,
        l => panic!("Length {}", l)
    }
}


#[test]
fn test_part_one() {
    assert_eq!(
        joltage_diff_multiplied(
            r#"
            16
            10
            15
            5
            1
            11
            7
            19
            6
            12
            4
            "#
        ),
        35
    );

    assert_eq!(
        joltage_diff_multiplied(
            r#"
            28
            33
            18
            42
            31
            14
            46
            20
            48
            47
            24
            23
            49
            45
            19
            38
            39
            11
            1
            32
            25
            35
            8
            17
            7
            9
            4
            2
            34
            10
            3
            "#
        ),
        220
    )
}

#[test]
fn test_part_two() {
    assert_eq!(joltage_permutations(
        r#"
        3 
        6
        7
        10
        11
        "#
    ), 1);

    assert_eq!(joltage_permutations(
        r#"
        16
        10
        15
        5
        1
        11
        7
        19
        6
        12
        4
        "#
    ), 8);



    assert_eq!(joltage_permutations(
        r#"
        28
        33
        18
        42
        31
        14
        46
        20
        48
        47
        24
        23
        49
        45
        19
        38
        39
        11
        1
        32
        25
        35
        8
        17
        7
        9
        4
        2
        34
        10
        3
        "#
    ), 19208);
}