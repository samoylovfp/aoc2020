use std::fs::read_to_string;

fn main() {
    let input = read_to_string("input.txt").unwrap();
    println!("First part is {}", count_trees(&input, 3, 1));
    let slopes = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
    println!(
        "Second part is {}",
        slopes
            .into_iter()
            .map(|(r, d)| count_trees(&input, r, d))
            .product::<usize>()
    );
}

fn count_trees(map: &str, right: usize, down: usize) -> usize {
    map
    .trim()
    .lines()
    .step_by(down)
    .enumerate()
    .map(|(y, line)|{
        line.chars().nth((y * right) % line.len())
    })
    .filter(|&c| c == Some('#'))
    .count()
}

#[test]
fn test_slopes() {
    let map = r#"
..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
"#;
    assert_eq!(count_trees(map, 3, 1), 7);
    assert_eq!(count_trees(map, 1, 2), 2);
}
