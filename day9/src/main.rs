use std::fs::read_to_string;

fn main() {
    let input = read_to_string("input.txt").unwrap();
    let invalid_number = find_first_invalid(&input, 25);
    println!(
        "Part one is {}", invalid_number
    );
    println!("Part two is {}", find_contiguous(&input, invalid_number));
}

fn find_first_invalid(data: &str, preamble: usize) -> u64 {
    let data: Vec<_> = data.lines().map(|l| l.trim().parse().unwrap()).collect();
    'search: for i in preamble..data.len() {
        for j in i - preamble..i {
            for k in i - preamble..i {
                if j != k && (data[j] + data[k]) == data[i] {
                    continue 'search;
                }
            }
        }
        return data[i];
    }
    panic!("Did not find the number");
}

fn find_contiguous(data: &str, value: u64) -> u64 {
    let data: Vec<u64> = data.lines().map(|l| l.trim().parse().unwrap()).collect();
    for i in 0..data.len() {
        let mut acc = 0;
        for j in i..data.len() {
            acc += data[j];
            if acc == value {
                return data[i..j].iter().min().unwrap() + data[i..j].iter().max().unwrap()
            }
            if acc > value {
                break;
            }
        }
    }
    panic!("Did not find the contiguous region")
}

#[test]
fn test_part_one() {
    let input = r#"35
    20
    15
    25
    47
    40
    62
    55
    65
    95
    102
    117
    150
    182
    127
    219
    299
    277
    309
    576"#;
    assert_eq!(find_first_invalid(input, 5), 127);
}

#[test]
fn part_two() {
    let input = r#"35
    20
    15
    25
    47
    40
    62
    55
    65
    95
    102
    117
    150
    182
    127
    219
    299
    277
    309
    576"#;
    assert_eq!(find_contiguous(input, 127), 62);
}