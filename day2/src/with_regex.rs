use std::fs::read_to_string;
use lazy_static::lazy_static;
use regex::Regex;

fn main() {
    let input = read_to_string("input.txt").unwrap();
    println!("Part 1 answer is {}", input.lines().filter(|l|is_valid_password(l)).count());
    println!("Part 2 answer is {}", input.lines().filter(|l|is_new_policy_password(l)).count());
}

fn is_valid_password(line: &str) -> bool {
    let (min, max, valid_char, value) = parse_reqs(line);
    let count = value.chars().filter(|&c| c == valid_char).count();
    count <= max && count >= min
}

fn is_new_policy_password(line: &str) -> bool {
    let (first, second, valid_char, value) = parse_reqs(line);
    (value.chars().nth(first - 1) == Some(valid_char)) ^
    (value.chars().nth(second - 1) == Some(valid_char))

}

fn parse_reqs(line: &str) -> (usize, usize, char, String) {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^(\d+)-(\d+) (\w): (\w+)$").unwrap();
    }
    let groups = RE.captures(line).unwrap();
    (
        groups[1].parse().unwrap(),
        groups[2].parse().unwrap(),
        groups[3].chars().next().unwrap(),
        groups[4].to_string()
    )
}


#[test]
fn test_first_part() {
    assert!(is_valid_password("1-3 a: abcde"));
    assert!(is_valid_password("2-9 c: ccccccccc"));

    assert!( ! is_valid_password("1-3 b: cdefg"));
}

#[test]
fn test_second_part() {
    assert!(is_new_policy_password("1-3 a: abcde"));

    assert!(! is_new_policy_password("1-3 b: cdefg"));
    assert!(! is_new_policy_password("2-9 c: ccccccccc"));
}