use std::{iter::Peekable, fs::read_to_string};


fn main() {
    let input = read_to_string("input.txt").unwrap();
    println!("Part 1 answer is {}", input.lines().filter(|l|is_valid_password(l)).count());
    println!("Part 2 answer is {}", input.lines().filter(|l|is_new_policy_password(l)).count());
}

fn is_valid_password(line: &str) -> bool {
    let mut symbols = line.chars().peekable();
    let min = read_int(&mut symbols);
    assert_eq!(symbols.next(), Some('-'));
    let max = read_int(&mut symbols);
    assert_eq!(symbols.next(), Some(' '));
    let validation_char = symbols.next().unwrap();
    let actual_count = symbols.filter(|&c|c==validation_char).count();

    actual_count >= min && actual_count <= max
}

fn is_new_policy_password(line: &str) -> bool {
    let mut symbols = line.chars().peekable();
    let first_pos = read_int(&mut symbols);
    assert_eq!(symbols.next(), Some('-'));
    let second_pos = read_int(&mut symbols);
    assert_eq!(symbols.next(), Some(' '));
    let validation_char = symbols.next().unwrap();
    assert_eq!(symbols.next(), Some(':'));
    assert_eq!(symbols.next(), Some(' '));

    (symbols.clone().nth(first_pos - 1) == Some(validation_char)) ^
    (symbols.nth(second_pos - 1) == Some(validation_char))
}

fn read_int(it: &mut Peekable<impl Iterator<Item=char>>) -> usize {
    let mut buffer = String::with_capacity(2);
    while it.peek().unwrap().is_ascii_digit() {
        buffer.push(it.next().unwrap())
    }
    buffer.parse().unwrap()
}

#[test]
fn test_first_part() {
    assert!(is_valid_password("1-3 a: abcde"));
    assert!(is_valid_password("2-9 c: ccccccccc"));

    assert!( ! is_valid_password("1-3 b: cdefg"));
}

#[test]
fn test_second_part() {
    assert!(is_new_policy_password("1-3 a: abcde"));

    assert!(! is_new_policy_password("1-3 b: cdefg"));
    assert!(! is_new_policy_password("2-9 c: ccccccccc"));
}