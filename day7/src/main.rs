#![feature(str_split_once)]

use std::{
    collections::{HashMap, HashSet},
    fs::read_to_string,
};

fn main() {
    let input = read_to_string("input.txt").unwrap();
    let rules = parse_tree(&input);
    println!(
        "First part is {}",
        count_parents(&rules.parents, "shiny gold")
    );
    println!(
        "Second part is {}",
        count_cumulative_children(&rules.children, "shiny gold")
    );
}

type Relations<'data> = HashMap<&'data str, Vec<(usize, &'data str)>>;

#[derive(Debug)]
struct Rules<'data> {
    parents: Relations<'data>,
    children: Relations<'data>,
}

fn parse_tree(data: &str) -> Rules {
    let mut parents: Relations = Relations::new();
    let mut children: Relations = Relations::new();
    for line in data.lines() {
        let (outer_bag, inner_bags) = line.split_once(" bags contain ").unwrap();
        if inner_bags != "no other bags." {
            let inner_bags = inner_bags.split(", ").map(parse_bag_contents);
            for (num, color) in inner_bags {
                parents.entry(color).or_default().push((num, outer_bag));
                children.entry(outer_bag).or_default().push((num, color));
            }
        }
    }
    Rules { parents, children }
}

fn parse_bag_contents(data: &str) -> (usize, &str) {
    let number = data.split_whitespace().next().unwrap().parse().unwrap();

    let mut space_indices = data.match_indices(" ");
    let start = space_indices.nth(0).unwrap().0 + 1;
    let end = space_indices.nth(1).unwrap().0;
    let color = &data[start..end];

    (number, color)
}

fn count_parents(parents: &Relations, element: &str) -> usize {
    let mut all_parents: HashSet<&str> = HashSet::new();
    let mut front: Vec<&str> = Vec::new();
    all_parents.extend(parents[element].iter().map(|(_, color)| color));
    front.extend(parents[element].iter().map(|(_, color)| color));
    while let Some(e) = front.pop() {
        if let Some(elements) = parents.get(e) {
            let colors = elements.iter().map(|(_, color)| color);
            all_parents.extend(colors.clone());
            front.extend(colors);
        }
    }
    all_parents.len()
}

fn count_cumulative_children(children: &Relations, element: &str) -> usize {
    children
        .get(element)
        .map(|bag_children| {
            bag_children
                .iter()
                .map(|(num, child_color)| {
                    num * (1 + count_cumulative_children(children, child_color))
                })
                .sum()
        })
        .unwrap_or(0)
}
