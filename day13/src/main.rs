#![feature(str_split_once)]

use std::fs::read_to_string;

fn main() {
    let input = read_to_string("input.txt").unwrap();
    println!("Part one is {}", part_one(&input));
    println!("Part two is {}", part_two(&input))
}

fn part_one(input: &str) -> u32 {
    let (your_delay, buses) = input.trim().split_once("\n").unwrap();
    let your_delay = your_delay.trim().parse::<u32>().unwrap();
    let (earliest_bus, delay) = buses
        .trim()
        .split(",")
        .filter(|&b| b != "x")
        .map(|b| b.parse::<u32>().unwrap())
        .map(|b| (b, b - (your_delay % b)))
        .min_by_key(|&(_, min_delay)| min_delay)
        .unwrap();

    earliest_bus * delay
}

#[derive(Debug)]
struct Bus {
    delay: u64,
    mul: u64,
}

fn part_two(input: &str) -> u64 {
    let (_your_delay, buses) = input.trim().split_once("\n").unwrap();
    let buses_delays = buses
        .trim()
        .split(",")
        .enumerate()
        .filter(|&(_delay, b)| b != "x")
        .map(|(d, bus)| Bus {
            delay: d as u64,
            mul: bus.parse::<u64>().unwrap(),
        })
        .collect::<Vec<_>>();

    let mut bus_it = buses_delays.iter();
    let mut current_bus = bus_it.next().unwrap();
    let mut current_adder = current_bus.mul;
    let mut current_time = 0;
    current_bus = bus_it.next().unwrap();

    loop {
        if (current_time + current_bus.delay) % current_bus.mul == 0 {
            if let Some(b) = bus_it.next() {
                current_adder *= current_bus.mul;
                current_bus = b;
            } else {
                return current_time;
            }
        }
        current_time += current_adder
    }
}

#[test]
fn test_part_one() {
    assert_eq!(
        part_one(
            r#"
        939
        7,13,x,x,59,x,31,19
    "#
        ),
        295
    );
}

#[test]
fn test_part_two() {
    assert_eq!(
        part_two(
            r#"
        939
        7,13,x,x,59,x,31,19
    "#
        ),
        1068781
    )
}
