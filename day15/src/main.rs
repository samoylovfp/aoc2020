use std::{collections::HashMap, fs::read_to_string};

fn main() {
    let input = read_to_string("input.txt").unwrap();
    let numbers: Vec<u32> = input.trim().split(',').map(|l|l.parse().unwrap()).collect();
    println!("First part is {}", say_numbers(&numbers, 2020));
    println!("Second part is {}", say_numbers(&numbers, 30000000));
}


fn say_numbers(input: &[u32], end: usize) -> u32 {
    let mut utterances = HashMap::<u32, usize>::new();
    let mut next_message = 0;
    let mut last_spoken = 0;

    for turn in 1..=end {
        last_spoken = input.get(turn - 1).copied().unwrap_or(next_message);

        next_message = match utterances.get(&last_spoken) {
            Some(n) => {(turn - n) as u32},
            None => 0
        };
        utterances.insert(last_spoken, turn);
    }
    last_spoken
}


#[test]
fn part_one() {
    assert_eq!(say_numbers(&[0, 3, 6], 4), 0);
    assert_eq!(say_numbers(&[0, 3, 6], 5), 3);
    assert_eq!(say_numbers(&[0, 3, 6], 6), 3);
    assert_eq!(say_numbers(&[1, 3, 2], 2020), 1);
}
