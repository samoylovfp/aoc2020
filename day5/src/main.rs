use std::fs::read_to_string;

fn main() {
    let input = read_to_string("input.txt").unwrap();
    println!(
        "First part is {}",
        input.lines().map(|l| boarding_pass_to_id(l)).max().unwrap()
    );
    println!("Second part is {}", find_missing(&input))
}

fn find_missing(passes: &str) -> u32 {
    let mut ids = passes.lines().map(boarding_pass_to_id).collect::<Vec<_>>();
    ids.sort();
    let first_id = ids[0];
    ids.into_iter()
        .zip(first_id..)
        .find(|(expected, actual)| expected != actual)
        .unwrap()
        .1
}

fn boarding_pass_to_id(boarding_pass: &str) -> u32 {
    bsp_to_number(&boarding_pass[0..7]) as u32 * 8 + bsp_to_number(&boarding_pass[7..]) as u32
}

fn bsp_to_number(num: &str) -> u8 {
    let binary = num
        .chars()
        .map(|c| match c {
            'R' | 'B' => '1',
            'L' | 'F' => '0',
            c => panic!("unknown char {:?}", c),
        })
        .collect::<String>();
    u8::from_str_radix(&binary, 2).unwrap()
}

#[test]
fn test_part_one() {
    assert_eq!(bsp_to_number("BFFFBBF"), 70);
    assert_eq!(bsp_to_number("RRR"), 7);
    assert_eq!(bsp_to_number("RLL"), 4);

    assert_eq!(boarding_pass_to_id("BFFFBBFRRR"), 567);
    assert_eq!(boarding_pass_to_id("FFFBBBFRRR"), 119);
}
