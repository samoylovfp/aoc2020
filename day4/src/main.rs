use std::{collections::HashMap, fs::read_to_string};

fn main() {
    let input = read_to_string("input.txt").unwrap();
    println!(
        "First part is {}",
        input
            .split("\n\n")
            .filter(|l| passport_has_all_keys(l))
            .count()
    );
    println!(
        "Second part is {}",
        input
            .split("\n\n")
            .filter(|l| passport_has_valid_data(l))
            .count()
    );
}

fn passport_has_all_keys(data: &str) -> bool {
    let fields = parse_passport(data);

    for required_field in vec!["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"] {
        if !fields.contains_key(required_field) {
            return false;
        }
    }

    true
}

fn parse_passport(data: &str) -> HashMap<String, String> {
    data.split_ascii_whitespace()
        .map(|kv| {
            let mut it = kv.split(":");
            (it.next().unwrap().to_owned(), it.next().unwrap().to_owned())
        })
        .collect()
}

fn passport_has_valid_data(data: &str) -> bool {
    let fields = parse_passport(data);
    let valid_year =
        |key, min, max| fields.get(key).map(|v| is_valid_number(v, min, max)) == Some(true);

    valid_year("byr", 1920, 2002)
        && valid_year("iyr", 2010, 2020)
        && valid_year("eyr", 2020, 2030)
        && (fields.get("hgt").map(|h| is_valid_height(h)) == Some(true))
        && (fields.get("hcl").map(|c| is_valid_col(c)) == Some(true))
        && (fields
            .get("ecl")
            .map(|c| vec!["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(&c.as_str()))
            == Some(true))
        && (fields
            .get("pid")
            .map(|p| p.len() == 9 && p.chars().all(|c| c.is_ascii_digit()))
            == Some(true))
}

fn is_valid_number(num: &str, min: u32, max: u32) -> bool {
    match num.parse::<u32>() {
        Ok(n) if n >= min && n <= max => true,
        _ => false,
    }
}

fn is_valid_height(val: &str) -> bool {
    let num_len = val.len().saturating_sub(2);
    match (
        val.get(0..num_len).and_then(|v| v.parse::<u32>().ok()),
        val.get(num_len..),
    ) {
        (Some(cm), Some("cm")) => cm >= 150 && cm <= 193,
        (Some(i), Some("in")) => i >= 59 && i <= 76,
        _ => false,
    }
}

fn is_valid_col(val: &str) -> bool {
    if let (Some("#"), Some(num)) = (val.get(0..1), val.get(1..)) {
        num.len() == 6
            && num
                .chars()
                .all(|c| c.is_ascii_hexdigit() && !c.is_ascii_uppercase())
    } else {
        false
    }
}

#[test]
fn test_part1() {
    assert!(passport_has_all_keys(
        r#"
        ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
        byr:1937 iyr:2017 cid:147 hgt:183cm
    "#
    ));

    assert!(passport_has_all_keys(
        r#"    
        hcl:#ae17e1 iyr:2013
        eyr:2024
        ecl:brn pid:760753108 byr:1931
        hgt:179cm
    "#
    ));

    assert!(!passport_has_all_keys(
        r#"
        iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
        hcl:#cfa07d byr:1929
    "#
    ));

    assert!(is_valid_height("60in"));
    assert!(is_valid_height("190cm"));

    assert!(!is_valid_height("20in"));
    assert!(!is_valid_height("2n"));
    assert!(!is_valid_height(""));

    assert!(is_valid_col("#1234ff"))
}
