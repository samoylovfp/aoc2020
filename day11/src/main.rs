use std::fs::read_to_string;

fn main() {
    let seats = parse_seats(&read_to_string("input.txt").unwrap());
    println!("Part one is {}", count_stable_state(seats.clone(), (0, 4), false));
    println!("Part two is {}", count_stable_state(seats, (0, 5), true));
}

type Seats = Vec<Vec<Cell>>;

fn count_stable_state(mut seats: Seats, rules: (usize, usize), look: bool) -> usize {
    turn_until_stable(&mut seats, rules, look);
    seats
        .iter()
        .flat_map(|l| l.iter())
        .filter(|&&c| c == Occupied)
        .count()
}

fn turn_until_stable(seats: &mut Seats, rules: (usize, usize), look: bool) {
    let mut s2 = seats.clone();
    loop {
        turn_passengers(&seats, &mut s2, rules, look);
        std::mem::swap(&mut s2, seats);
        if seats == &s2 {
            return;
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Cell {
    Floor,
    Vacant,
    Occupied,
}


use Cell::*;

fn parse_seats(input: &str) -> Seats {
    input
        .trim()
        .lines()
        .map(|line| {
            line.trim()
                .chars()
                .map(|c| match c {
                    '.' => Floor,
                    'L' => Vacant,
                    '#' => Occupied,
                    c => panic!("Unknown char {:?}", c),
                })
                .collect()
        })
        .collect()
}

fn turn_passengers(source: &Seats, target: &mut Seats, rules: (usize, usize), look: bool) {
    for (y, line) in source.iter().enumerate() {
        for (x, &cell) in line.iter().enumerate() {
            target[y][x] = cell;
            match cell {
                Floor => {}
                Vacant => {
                    if count_neighbours(source, x, y, look) <= rules.0 {
                        target[y][x] = Occupied
                    }
                }
                Occupied => {
                    if count_neighbours(source, x, y, look) >= rules.1 {
                        target[y][x] = Vacant
                    }
                }
            }
        }
    }
}

fn count_neighbours(source: &Seats, x: usize, y: usize, look: bool) -> usize {
    static DIRS: [(i32, i32); 8] = [
        (-1, -1),
        (-1, 0),
        (-1, 1),
        (0, 1),
        (0, -1),
        (1, -1),
        (1, 0),
        (1, 1),
    ];

    if look {
        DIRS.iter()
            .map(|&(dx, dy)| {
                (1..)
                    .map(|i| (x as i32 + dx * i, y as i32 + dy * i))
                    .take_while(|&(absx, absy)| absx >= 0 && absy >= 0)
                    .map(|(x, y)| source.get(y as usize).and_then(|l| l.get(x as usize)))
                    .find(|&c| c != Some(&Floor))
                    .flatten()
            })
            .filter(|&c| c == Some(&Occupied))
            .count()
    } else {
        DIRS.iter()
            .map(|&(dx, dy)| (x as i32 + dx, y as i32 + dy))
            .filter_map(|(absx, absy)| {
                if absx >= 0 && absy >= 0 {
                    source.get(absy as usize).and_then(|l| l.get(absx as usize))
                } else {
                    None
                }
            })
            .filter(|&c| c == &Occupied)
            .count()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn assert_equal(actual: &Seats, expected: &Seats) {
        if actual != expected {
            assert_eq!(expected.len(), actual.len());
            for (l1, l2) in expected.iter().zip(actual) {
                for (c1, c2) in l1.iter().zip(l2) {
                    if c1 != c2 {
                        match c2 {
                            Occupied => print!("v"),
                            Vacant => print!("^"),
                            Floor => print!("X"),
                        }
                    } else {
                        print!("{}", c1.to_char())
                    }
                }
                println!("");
            }
            println!("");
            assert!(false);
        }
    }

    impl Cell {
        fn to_char(&self) -> char {
            match self {
                Floor => '.',
                Vacant => 'L',
                Occupied => '#',
            }
        }
    }

    #[test]
    fn test_part_one() {
        let mut seats = parse_seats(
            r#"
            L.LL.LL.LL
            LLLLLLL.LL
            L.L.L..L..
            LLLL.LL.LL
            L.LL.LL.LL
            L.LLLLL.LL
            ..L.L.....
            LLLLLLLLLL
            L.LLLLLL.L
            L.LLLLL.LL
        "#,
        );
        let mut seats2 = seats.clone();
        turn_passengers(&seats, &mut seats2, (0, 4), false);
        assert_equal(
            &seats2,
            &parse_seats(
                r#"
            #.##.##.##
            #######.##
            #.#.#..#..
            ####.##.##
            #.##.##.##
            #.#####.##
            ..#.#.....
            ##########
            #.######.#
            #.#####.##
        "#,
            ),
        );
        turn_passengers(&seats2, &mut seats, (0, 4), false);
        assert_equal(
            &seats,
            &parse_seats(
                r#"
            #.LL.L#.##
            #LLLLLL.L#
            L.L.L..L..
            #LLL.LL.L#
            #.LL.LL.LL
            #.LLLL#.##
            ..L.L.....
            #LLLLLLLL#
            #.LLLLLL.L
            #.#LLLL.##
        "#,
            ),
        );
        assert_eq!(count_stable_state(seats, (0, 4), false), 37);
    }

    #[test]
    fn test_raytrace_passengers() {
        let p = parse_seats(
            r#"
        .......#.
        ...#.....
        .#.......
        .........
        ..#L....#
        ....#....
        .........
        #........
        ...#.....
        "#,
        );
        assert_eq!(count_neighbours(&p, 3, 4, true), 8);
    }
    #[test]
    fn test_part_two() {
        let mut s = parse_seats(
            r#"
            #.##.##.##
            #######.##
            #.#.#..#..
            ####.##.##
            #.##.##.##
            #.#####.##
            ..#.#.....
            ##########
            #.######.#
            #.#####.##
        "#,
        );
        turn_passengers(&s.clone(), &mut s, (0, 5), true);
        assert_equal(
            &s,
            &parse_seats(
                r#"
                #.LL.LL.L#
                #LLLLLL.LL
                L.L.L..L..
                LLLL.LL.LL
                L.LL.LL.LL
                L.LLLLL.LL
                ..L.L.....
                LLLLLLLLL#
                #.LLLLLL.L
                #.LLLLL.L#
            "#,
            ),
        );
    }
}