use std::fs::read_to_string;

fn main() {
    let input = read_to_string("input.txt").unwrap();
    println!("Hello!");
    println!("Part one is {}", get_coords_part_one(&input));
    println!("Part two is {}", get_coords_part_two(&input));
}

fn get_coords_part_one(input: &str) -> i32 {
    let instructions = input
        .trim()
        .lines()
        .map(|l| {
            (
                l.trim().chars().next().unwrap(),
                l.trim()[1..].parse().unwrap(),
            )
        })
        .collect::<Vec<(char, u32)>>();

    let mut heading_degrees = 0;
    let (mut x, mut y) = (0, 0);

    for (instr, length) in instructions {
        let length = length as i32;
        match instr {
            'N' => y += length,
            'E' => x += length,
            'W' => x -= length,
            'S' => y -= length,
            'F' => match heading_degrees {
                0 => x += length,
                90 => y += length,
                180 => x -= length,
                270 => y -= length,
                d => panic!("Non-90 degrees: {}", d),
            },
            'R' => heading_degrees = (heading_degrees - length + 360) % 360,
            'L' => heading_degrees = (heading_degrees + length) % 360,
            i => panic!("Don't know how to {}", i),
        }
        println!(
            "After executing {}:{} we are at {}:{} heading {}",
            instr, length, x, y, heading_degrees
        )
    }
    return x.abs() + y.abs();
}

fn turn((x, y): (&mut i32, &mut i32), deg: i32) {
    match deg {
        90 => {
            let tmp = *y;
            *y = *x;
            *x = -tmp;
        },
        180 => {
            *y = -*y;
            *x = -*x;
        },
        270 => {
            let tmp = *y;
            *y = -*x;
            *x = tmp
        },
        d => panic!("Unsupported turn {}", d)
    }
}


fn get_coords_part_two(input: &str) -> i32 {
    let instructions = input
        .trim()
        .lines()
        .map(|l| {
            (
                l.trim().chars().next().unwrap(),
                l.trim()[1..].parse().unwrap(),
            )
        })
        .collect::<Vec<(char, u32)>>();

    let (mut ship_x, mut ship_y) = (0, 0);
    let (mut wp_x, mut wp_y) = (10, 1);

    for (instr, length) in instructions {
        let length = length as i32;
        match instr {
            'N' => wp_y += length,
            'E' => wp_x += length,
            'W' => wp_x -= length,
            'S' => wp_y -= length,
            'F' => {
                ship_x += wp_x * length;
                ship_y += wp_y * length;
            },
            'R' => turn((&mut wp_x, &mut wp_y), 360 - length),
            'L' => turn((&mut wp_x, &mut wp_y), length),
            i => panic!("Don't know how to {}", i),
        }
        println!(
            "After executing {}:{} we are at {}:{}, wp at {}:{}",
            instr, length, ship_x, ship_y, wp_x, wp_y
        )
    }
    return ship_x.abs() + ship_y.abs();
}

#[test]
fn part_one() {
    let input = r#"F10
        N3
        F7
        R90
        F11
    "#;
    assert_eq!(get_coords_part_one(input), 25);
}

#[test]
fn part_two() {
    let input = r#"F10
    N3
    F7
    R90
    F11
"#;
    assert_eq!(get_coords_part_two(input), 286);
}
