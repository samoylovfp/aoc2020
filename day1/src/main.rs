fn main() {
    let input = std::fs::read_to_string("input.txt").unwrap();
    let mut numbers: Vec<u32> = input.lines().map(|l|l.parse().unwrap()).collect();
    numbers.sort();
    'part1: for n1 in &numbers {
        for n2 in &numbers {
            if n1 + n2 == 2020 {
                println!("First part answer is {}", n1 * n2);
                break 'part1;    
            }
            if n1 + n2 > 2020 {
                break
            }
        }
    }
    'part2: for n1 in &numbers {
        for n2 in &numbers {
            let s12 = n1 + n2;
            if s12 >= 2020 {
                break;
            }
            for n3 in &numbers {
                let s123 = s12 + n3;
                if s123 == 2020 {
                    println!("Part2 is {}", n1 * n2 * n3);
                    break 'part2;
                }
                if s123 > 2020 {
                    break;
                }
            }
        }
    }
}
