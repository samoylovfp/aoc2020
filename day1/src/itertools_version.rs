use itertools::Itertools;

fn main() {
    let input = std::fs::read_to_string("input.txt").unwrap();
    let numbers: Vec<u32> = input.lines().map(|l|l.parse().unwrap()).collect();
    for n in vec![2, 3] {
        println!("For number {} response is {}", n, numbers.iter()
            .combinations(n)
            .find(|cmb| cmb.iter().copied().sum::<u32>() == 2020)
            .map(|cmb| cmb.iter().copied().product::<u32>())
            .unwrap()
        );
    }
}
