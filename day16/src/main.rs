use std::fs::read_to_string;

#[derive(Debug)]
struct Field {
    name: String,
    ranges: Vec<(u32, u32)>,
}

#[derive(Debug)]
struct Task {
    fields: Vec<Field>,
    my_ticket: Vec<u32>,
    neighbor_tickets: Vec<Vec<u32>>,
}

fn main() {
    let input = read_to_string("input.txt").unwrap();
    println!("Part one is {}", part_one(&input));
}

fn part_one(input: &str) -> u64 {
    let t = parse_task(input);
    t.neighbor_tickets
        .iter()
        .map(|ticket| {
            ticket
                .iter()
                .filter(|&n| {
                    t.fields
                        .iter()
                        .all(|f| f.ranges.iter().all(|&(min, max)| *n < min || *n > max))
                })
                .sum::<u32>() as u64
        })
        .sum()
}

fn parse_task(input: &str) -> Task {
    let mut it = input.split("your ticket:");
    let fields = it
        .next()
        .unwrap()
        .trim()
        .lines()
        .map(|l| {
            let mut it = l.split(": ");
            let name = it.next().unwrap().trim().to_string();
            let ranges = it
                .next()
                .unwrap()
                .split(" or ")
                .map(|i| {
                    let mut it = i.split("-");
                    (
                        it.next().unwrap().trim().parse().unwrap(),
                        it.next().unwrap().trim().parse().unwrap(),
                    )
                })
                .collect();
            Field { name, ranges }
        })
        .collect();
    let mut it = it.next().unwrap().split("nearby tickets:");
    let my_ticket = it
        .next()
        .unwrap()
        .trim()
        .split(",")
        .map(|n| n.parse().unwrap())
        .collect();
    let neighbor_tickets = it
        .next()
        .unwrap()
        .trim()
        .lines()
        .map(|l| l.split(",").map(|n| n.trim().parse().unwrap()).collect())
        .collect();

    Task {
        fields,
        my_ticket,
        neighbor_tickets,
    }
}

#[test]
fn test_part_one() {
    assert_eq!(
        part_one(
            r#"
        class: 1-3 or 5-7
        row: 6-11 or 33-44
        seat: 13-40 or 45-50

        your ticket:
        7,1,14

        nearby tickets:
        7,3,47
        40,4,50
        55,2,20
        38,6,12
    "#
        ),
        71
    );
}
