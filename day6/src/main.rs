#![feature(iterator_fold_self)]
use std::{collections::HashSet, fs::read_to_string};

fn main() {
    let input = read_to_string("input.txt").unwrap();
    println!(
        "part 1: {}",
        input
            .replace("\r\n", "\n")
            .split("\n\n")
            .map(|group| group
                .chars()
                .filter(|c| !c.is_whitespace())
                .collect::<HashSet<_>>()
                .len())
            .sum::<usize>()
    );

    println!(
        "part 2: {}",
        input
            .replace("\r\n", "\n")
            .split("\n\n")
            .map(|group| group
                .lines()
                .map(|person| person.chars().collect::<HashSet<_>>())
                .fold_first(|p1, p2| p1.intersection(&p2).copied().collect())
                .unwrap()
                .len())
            .sum::<usize>()
    );
}
